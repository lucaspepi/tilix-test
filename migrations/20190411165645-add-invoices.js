'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  db.createTable('invoices', {
    id: { type: 'int', primaryKey: true, notNull: true, autoIncrement: true },
    trading_name: { type: 'string', notNull: true},
    due_date: { type: 'date', notNull: true},
    price: { type: 'decimal', notNull: true, length: '10, 2' },
    paid: { type: 'boolean', notNull: true },
    user_id: { type: 'int', notNull: true }
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('invoices', callback);
};

exports._meta = {
  "version": 1
};
