# Tilix API Test

## Installation

To install dependencies run command:

```bash
npm install
```

## Config

Create a new file called .env, copy the content of .env.example and set the information from your database.

Create the database with the same name of DB_DATABASE variable the .env file

Command to run migrations:
```bash
node node_modules/db-migrate/bin/db-migrate up
```

## Setup

Command to run server:
```bash
node app.js
```

## Test

Command to run tests:
```bash
npm run test
```