const request = require('supertest')
const server = require('../app.js')

beforeAll(async () => {
 // do something before anything else runs
 console.log('Jest starting!')
})

// close the server after each test
afterAll(() => {
    console.log('server closed!')
})

describe('basic route tests', () => {
    test('get all invoices', async () => {
        const response = await request(server).get('/invoices')
        expect(response.status).toEqual(200)
    })

    test('post invoice', async () => {
        const invoice = {
            user_id: 1,
            trading_name: "Teste",
            price: 100,
            due_date: "2019-04-10",
            paid: true
        }
        const response = await request(server).post('/invoices').send(invoice)
        expect(response.status).toEqual(200)
    })

    test('get one invoice', async () => {
        const response = await request(server).get('/invoices/1')
        expect(response.status).toEqual(200)
    })

    test('put invoice', async () => {
        const invoice = {
            user_id: 1,
            trading_name: "Teste Update",
            price: 100,
            due_date: "2019-04-10",
            paid: true
        }
        const response = await request(server).put('/invoices/1').send(invoice)
        expect(response.status).toEqual(200)
    })

    test('delete invoice', async () => {
        const response = await request(server).delete('/invoices/1')
        expect(response.status).toEqual(200)
    })
})