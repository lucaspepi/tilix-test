const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const mysql = require('mysql')
const cors = require('cors')

require('dotenv').config()

const db = mysql.createConnection ({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    timezone: 'utc'
})

app.use(cors())
app.use(bodyParser.json())

db.connect((err) => {
    if (err) {
        throw err
    }
    console.log('Connected to database')
})
global.db = db

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
  require('./src/routes/invoice.js')(app)
})

module.exports = app
