const Invoice = require('../controllers/invoicesController')
const validate = require('express-validation')
const validation = require('../requests/invoice')

module.exports = api => {
	api.route('/invoices').get(Invoice.index)
	api.route('/invoices/:invoiceId').get(Invoice.show)
	api.route('/invoices/:invoiceId').put(validate(validation), Invoice.put)
	api.route('/invoices').post(validate(validation), Invoice.post)
	api.route('/invoices/:invoiceId').delete(Invoice.delete)

	api.use(function(err, req, res, next){
		res.status(400).json(err);
	})
}