const messages = require('../utils/messages')

module.exports = {
    index: (req, res) => {
        let query = "SELECT * FROM invoices"

        // execute query
        db.query(query, (err, result) => {
            if (err) {
                res.json(err)
            }

            res.json(result)
        })
    },

    show: (req, res) => {
        let query = 'SELECT * FROM invoices where id = ?'

        // execute query
        db.query(query, req.params.invoiceId, (err, result) => {
            if (err) {
                res.json(err)
            }

            res.json(result[0])
        })
    },

    put: (req, res) => {
        const invoice = {
            trading_name: req.body.trading_name,
            due_date: req.body.due_date,
            price: req.body.price,
            paid: req.body.paid,
            user_id: req.body.user_id
        }

        let query = 'UPDATE invoices SET ? WHERE id = ?'

        // execute query
        db.query(query, [invoice, req.params.invoiceId], (err, result) => {
            if (err) {
                res.json(err)
            }

            res.json({
                msg: messages.update
            })
        })
    },

    post: (req, res) => {
        const invoice = {
            trading_name: req.body.trading_name,
            due_date: req.body.due_date,
            price: req.body.price,
            paid: req.body.paid,
            user_id: req.body.user_id
        }

        let query = 'INSERT INTO invoices set ?'

        // execute query
        db.query(query, invoice, (err, result) => {
            if (err) {
                res.json(err)
            }

            res.json({
                msg: messages.insert
            })
        })
    },

    delete: (req, res) => {
        let query = 'DELETE FROM invoices where id = ?'

        // execute query
        db.query(query, req.params.invoiceId, (err, result) => {
            if (err) {
                res.json(err)
            }

            res.json({
                msg: messages.delete
            })
        })
    }
}