const Invoice = require('../models/invoice')

module.exports = {
    index: (req, res) => {
        Invoice.index(req, res)
    },

    show: (req, res) => {
        Invoice.show(req, res)
    },

    put: (req, res) => {
        Invoice.put(req, res)
    },

    post: (req, res) => {
        Invoice.post(req, res)
    },

    delete: (req, res) => {
        Invoice.delete(req, res)
    }
}