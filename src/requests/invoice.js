const Joi = require('joi');
 
module.exports = {
  body: {
    trading_name: Joi.string().required().error(() => 'O campo empresa é obrigatório.'),
    due_date: Joi.date().required().error(() => 'O campo data de vencimento é obrigatório.'),
    price: Joi.number().required().error(() => 'O campo preço é obrigatório.'),
    paid: Joi.boolean().required()
  }
}